<div>
    <p>Dear {{$user->name}},</p>
    <p>{{$currency->name}} exchange rate has been changed from {{$oldRate}} to {{$currency->rate}}!</p>
    <p>Thanks,</p>
    <p>Crypto Market Service!</p>
</div>