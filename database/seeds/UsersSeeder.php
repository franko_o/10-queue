<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password1'),
                'is_admin' => 1
            ],
            [
                'name' => 'customer',
                'email' => 'customer@customer.com',
                'password' => bcrypt('password1'),
                'is_admin' => 0
            ]
        ];
        \App\User::query()->insert($data);
    }
}
