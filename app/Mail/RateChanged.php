<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Entity\Currency;

class RateChanged extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    protected $user;
    /**
     * @var Currency
     */
    protected $currency;
    /**
     * @var float
     */
    protected $oldRate;

    /**
     * RateChanged constructor.
     * @param User $user
     * @param Currency $currency
     * @param float $oldRate
     */
    public function __construct(User $user, Currency $currency, float $oldRate)
    {
        $this->user = $user;
        $this->currency = $currency;
        $this->oldRate = $oldRate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.notification')
            ->with([
                'user' => $this->user,
                'currency' => $this->currency,
                'oldRate' => $this->oldRate,
            ]);
    }
}
