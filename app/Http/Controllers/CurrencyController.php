<?php

namespace App\Http\Controllers;

use App\Entity\Currency;
use App\Http\Requests\CurrencyRequest;
use App\Jobs\SendRateChangedEmail;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    public function update(CurrencyRequest $request, $id)
    {
        $currency = Currency::find($id);

        $oldRate = $currency->rate;

        // TODO: JWT AUTH
        /*if (! Auth::user()->can('update', $currency)) {
            return redirect('/');
        }*/

        if (! $currency) {
            return response()->json(['error']);
        }

        $currency->rate = $request->rate;
        $currency->save();

        $users = User::notAdmin()->get();
        foreach ($users as $user) {
            SendRateChangedEmail::dispatch($user, $currency, $oldRate)->onQueue('notification');
        }

        return response()->json($currency);
    }
}
